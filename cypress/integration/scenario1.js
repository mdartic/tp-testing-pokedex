describe('Home page', function () {
  it('render two divs correctly', function () {
    cy.visit('http://localhost:8080');
    cy.get('div.pokemon-detail').should('exist');
    cy.get('div.pokemon-list').should('exist');
  });
  it('render a table correctly', function () {
    cy.visit('http://localhost:8080');
    cy.get('div.pokemon-list table').should('exist');
    cy.get('div.pokemon-list table').contains('bulbasaur').should('exist');
    cy.get('div.pokemon-list table button').should('exist');
  });
  it('render the detail of a pokemon correctly', function () {
    cy.visit('http://localhost:8080');
    cy.get('div.pokemon-list table').should('exist');
    cy.get('div.pokemon-list table button').contains('Click for detail').click();
    cy.get('div.pokemon-detail').contains('bulbasaur').should('exist');
  });
})
