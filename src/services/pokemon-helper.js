/**
 * Transform a pokemon object with
 * several info that will be truncated or modified.
 *
 * @param {Object} pokemon
 * Object that has to be transformed,
 * @returns {Object}
 * { id, name, sprites, abilities, species, order, is_default, height, width, base_experience }
 */
export function transformPokemonResponse(pokemon) {
  const newPokemon = { ...pokemon };
  delete newPokemon.forms;
  delete newPokemon.moves;
  delete newPokemon.game_indices;
  delete newPokemon.location_area_encounters;
  delete newPokemon.held_items;
  delete newPokemon.stats;
  delete newPokemon.types;
  newPokemon.abilities = newPokemon.abilities.map(({ ability }) => ability.name);
  newPokemon.species = newPokemon.species.name;
  newPokemon.sprites = Object.keys(newPokemon.sprites)
    .map(key => newPokemon.sprites[key])
    .filter(v => v !== null);
  return newPokemon;
}

/**
 * Enhance the pokemons result [{ url, name }]
 * by adding an id calculated from url.
 *
 * @param {Array} pokemons
 * list of pokemons { url, name }
 *
 * @returns {Array}
 * list of pokemons { id, url, name }
 */
export function enhancePokemonsResponse(pokemons) {
  return pokemons.map((pokemon) => {
    const lastIndex = pokemon.url.lastIndexOf('/');
    const fromIndex = pokemon.url.lastIndexOf('/', lastIndex - 1);
    const id = pokemon.url.substr(fromIndex + 1, lastIndex - fromIndex - 1);
    return { id, ...pokemon };
  });
}

export default {
  transformPokemonResponse,
  enhancePokemonsResponse,
};
