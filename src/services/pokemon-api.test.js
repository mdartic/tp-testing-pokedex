import axios from 'axios';

import { getPokemon } from './pokemon-api';
import pokemonHelper from './pokemon-helper';

jest.mock('./pokemon-helper');
const id = 10;
const URL = 'https://pokeapi.co/api/v2';
let urlCalled = null;

describe('the getPokemon method', () => {
  beforeEach(() => {
    axios.get = jest.fn((url) => {
      urlCalled = url;
      return Promise.resolve({
        data: {
          abilities: [],
          species: { name: 'pouet' },
          sprites: {},
        },
      });
    });
  });

  test('call the default url', async () => {
    expect.assertions(2);
    await getPokemon(id);
    expect(urlCalled).toBe(`${URL}/pokemon/${id}/`);
    expect(axios.get.mock.calls.length).toEqual(1);
  });

  test('call transformPokemonResponse', async () => {
    expect.assertions(1);
    await getPokemon(id);
    expect(pokemonHelper.transformPokemonResponse).toBeCalled();
  });

  test('call transformPokemonResponse with the good param', async () => {
    expect.assertions(1);
    await getPokemon(id).then(() => {
      expect(pokemonHelper.transformPokemonResponse).toBeCalled();
    });
  });

  test('expect the catch works well', async () => {
    expect.assertions(1);
    axios.get = jest.fn(() => Promise.reject(new Error('error')));
    await getPokemon().catch((error) => {
      expect(error.message).toEqual('error');
    });
  });
});
