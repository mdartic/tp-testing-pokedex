import axios from 'axios';
import { transformPokemonResponse, enhancePokemonsResponse } from './pokemon-helper';

const URL = 'https://pokeapi.co/api/v2';

/**
 * Get Pokemons from Pokedex API v2
 * @returns Promise
 */
export const getPokemons = () => axios.get(`${URL}/pokemon/`)
  .then(data => enhancePokemonsResponse(data.data.results));

/**
 * Get detail info on a pokemon from Pokedex API v2
 * @returns Promise
 */
export const getPokemon = id => axios.get(`${URL}/pokemon/${id}/`)
  .then(data => transformPokemonResponse(data.data));

export default {
  getPokemons, getPokemon,
};
