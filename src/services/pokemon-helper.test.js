const { transformPokemonResponse } = require('./pokemon-helper');

const data1 = {
  forms: [{ url: 'https://pokeapi.co/api/v2/pokemon-form/6/', name: 'charizard' }],
  abilities: [
    { slot: 3, is_hidden: true, ability: { url: 'https://pokeapi.co/api/v2/ability/94/', name: 'solar-power' } },
    { slot: 1, is_hidden: false, ability: { url: 'https://pokeapi.co/api/v2/ability/66/', name: 'blaze' } },
  ],
  stats: [
    { stat: { url: 'https://pokeapi.co/api/v2/stat/6/', name: 'speed' }, effort: 0, base_stat: 100 },
    { stat: { url: 'https://pokeapi.co/api/v2/stat/5/', name: 'special-defense' }, effort: 0, base_stat: 85 },
    { stat: { url: 'https://pokeapi.co/api/v2/stat/4/', name: 'special-attack' }, effort: 3, base_stat: 109 },
    { stat: { url: 'https://pokeapi.co/api/v2/stat/3/', name: 'defense' }, effort: 0, base_stat: 78 },
    { stat: { url: 'https://pokeapi.co/api/v2/stat/2/', name: 'attack' }, effort: 0, base_stat: 84 },
    { stat: { url: 'https://pokeapi.co/api/v2/stat/1/', name: 'hp' }, effort: 0, base_stat: 78 },
  ],
  name: 'charizard',
  weight: 905,
  moves: [],
  sprites: {
    back_female: null,
    back_shiny_female: null,
    back_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/6.png',
    front_female: null,
    front_shiny_female: null,
    back_shiny: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/6.png',
    front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png',
    front_shiny: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/6.png',
  },
  held_items: [],
  location_area_encounters: '/api/v2/pokemon/6/encounters',
  height: 17,
  is_default: true,
  species: { url: 'https://pokeapi.co/api/v2/pokemon-species/6/', name: 'charizard' },
  id: 6,
  order: 7,
  game_indices: [],
  base_experience: 240,
  types: [{ slot: 2, type: { url: 'https://pokeapi.co/api/v2/type/3/', name: 'flying' } }, { slot: 1, type: { url: 'https://pokeapi.co/api/v2/type/10/', name: 'fire' } }],
};

test('transformPokemonResponse delete some properties', () => {
  const result = transformPokemonResponse(data1);
  expect(result.forms).toBeUndefined();
  expect(result.moves).toBeUndefined();
  expect(result.game_indices).toBeUndefined();
  expect(result.location_area_encounters).toBeUndefined();
  expect(result.held_items).toBeUndefined();
  expect(result.stats).toBeUndefined();
  expect(result.types).toBeUndefined();
});

test('transformPokemonResponse transform sprite Objet in an Array', () => {
  const result = transformPokemonResponse(data1);
  expect(result.sprites).not.toBe(Object);
  expect(Array.isArray(result.sprites)).toBe(true);
  expect(result.sprites).toContain('https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/6.png');
});

test('transformPokemonResponse transform species Objet in a string', () => {
  const result = transformPokemonResponse(data1);
  expect(result.species).not.toBe(Object);
  expect(result.species).toEqual(data1.species.name);
});
