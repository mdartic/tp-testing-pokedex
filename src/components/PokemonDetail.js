const dictionary = [
  'id', 'abilities',
  'species', 'order', 'is_default',
  'height', 'weight',
  'base_experience',
];

export default class PokemonDetail {
  constructor() {
    this.divComponent = document.createElement('div');
    this.divComponent.classList.add('pokemon-detail');
    document.body.appendChild(this.divComponent);
  }

  emptyDOM() {
    while (this.divComponent.firstChild) {
      this.divComponent.removeChild(this.divComponent.firstChild);
    }
  }

  setData = (result) => {
    // empty current element
    this.emptyDOM();

    // display big name
    const currentName = document.createElement('h2');
    currentName.innerHTML = result.name;
    this.divComponent.appendChild(currentName);

    // special case for sprites
    result.sprites.forEach((sprite) => {
      const currentSprite = document.createElement('img');
      currentSprite.src = sprite;
      this.divComponent.appendChild(currentSprite);
    });
    // each property is displayed
    dictionary.forEach((property) => {
      const currentValue = result[property];
      const elementProperty = document.createElement('div');
      elementProperty.classList.add('property');
      elementProperty.innerHTML = `<b>${property}</b> : ${currentValue}`;
      this.divComponent.appendChild(elementProperty);
    });
  };
}
