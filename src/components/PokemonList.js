export default class PokemonList {
  constructor(triggerFunction) {
    this.init();
    this.triggerFunction = triggerFunction;
  }

  init() {
    if (this.tableComponent) { this.tableComponent.remove(); }
    this.tableComponent = document.createElement('table');
    this.responsiveTableComponent = document.createElement('div');
    this.responsiveTableComponent.classList.add('responsive');
    this.responsiveTableComponent.classList.add('pokemon-list');
    this.responsiveTableComponent.appendChild(this.tableComponent);
    document.body.appendChild(this.responsiveTableComponent);
  }

  appendTableHead(header) {
    const elementTr = document.createElement('tr');
    Object.keys(header).forEach((key) => {
      const elementTh = document.createElement('th');
      elementTh.innerText = key;
      elementTr.appendChild(elementTh);
    });
    const elementTh = document.createElement('th');
    elementTh.innerText = 'detail';
    elementTr.appendChild(elementTh);
    this.tableComponent.appendChild(elementTr);
  }

  appendTableRows(data) {
    data.forEach((currentData) => {
      const elementTr = document.createElement('tr');
      Object.keys(currentData).forEach((key) => {
        const elementTd = document.createElement('td');
        elementTd.innerText = currentData[key];
        elementTr.appendChild(elementTd);
      });
      const elementTd = document.createElement('td');
      const elementButton = document.createElement('button');
      elementButton.innerText = 'Click for detail';
      elementButton.addEventListener('click', () => {
        this.triggerFunction(currentData.id);
      });
      elementTd.appendChild(elementButton);
      elementTr.appendChild(elementTd);
      this.tableComponent.appendChild(elementTr);
    });
  }

  setData = (results) => {
    this.appendTableHead(results[0]);
    this.appendTableRows(results);
  };
}
