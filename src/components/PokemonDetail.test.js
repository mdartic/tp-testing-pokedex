import PokemonDetail from './PokemonDetail';

const bulbasaur = {
  abilities: [
    'chlorophyll',
    'overgrow',
  ],
  name: 'bulbasaur',
  weight: 69,
  sprites: [
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/1.png',
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/1.png',
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png',
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/1.png',
  ],
  height: 7,
  is_default: true,
  species: 'bulbasaur',
  id: 1,
  order: 1,
  base_experience: 64,
};

const blastoise = {
  abilities: [
    'rain-dish',
    'torrent',
  ],
  name: 'blastoise',
  weight: 855,
  sprites: [
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/9.png',
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/9.png',
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/9.png',
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/9.png',
  ],
  height: 16,
  is_default: true,
  species: 'blastoise',
  id: 9,
  order: 12,
  base_experience: 239,
};

describe('the PokemonDetail component', () => {
  test('has a setData method', () => {
    const component = new PokemonDetail();
    expect(component.setData).toBeDefined();
  });
  test('has a emptyDOM method', () => {
    const component = new PokemonDetail();
    expect(component.emptyDOM).toBeDefined();
  });
  test('has a divComponent object', () => {
    const component = new PokemonDetail();
    expect(component.divComponent).toBeDefined();
  });
  test('match an empty snapshot', () => {
    const component = new PokemonDetail();
    expect(component.divComponent).toMatchSnapshot();
  });
  test('match a snapshot with pokemon bulbasaur', () => {
    const component = new PokemonDetail();
    component.setData(bulbasaur);
    expect(component.divComponent).toMatchSnapshot();
  });
  test('match a snapshot after two display', () => {
    const component = new PokemonDetail();
    component.setData(bulbasaur);
    component.setData(blastoise);
    expect(component.divComponent).toMatchSnapshot();
  });
});
