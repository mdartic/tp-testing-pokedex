/* global Raven */
/* eslint no-console: ["error", { allow: ["log", "error"] }] */
import './styles/main.css';

import { getPokemons, getPokemon } from './services/pokemon-api';

import PokemonList from './components/PokemonList';
import PokemonDetail from './components/PokemonDetail';

const PokemonDetailComponent = new PokemonDetail();

const getPokemonDetail = (id) => {
  getPokemon(id).then((pokemonDetail) => {
    PokemonDetailComponent.setData(pokemonDetail);
  });
};

const PokemonListComponent = new PokemonList(getPokemonDetail);
getPokemons().then((pokemonsListing) => {
  PokemonListComponent.setData(pokemonsListing);
});

Raven.captureMessage('Something happened', {
  level: 'info', // one of 'info', 'warning', or 'error'
});

Raven.captureMessage('Something happened and break something', {
  level: 'error', // one of 'info', 'warning', or 'error'
});

throw new Error('Erreur non catchée');
