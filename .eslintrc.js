module.exports = {
  "extends": "airbnb-base",
  "env": {
    "browser": true,
    "jest": true
  },
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaVersion": 6
  },

};
