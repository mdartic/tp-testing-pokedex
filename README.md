# TP Testing Pokedex

[![Build status](https://gitlab.com/mdartic/tp-testing-pokedex/badges/ci-cd-correction/build.svg)](https://gitlab.com/mdartic/tp-testing-pokedex/commits/ci-cd-correction)
[![Overall test coverage](https://gitlab.com/mdartic/tp-testing-pokedex/badges/ci-cd-correction/coverage.svg)](https://gitlab.com/mdartic/tp-testing-pokedex/pipelines)


Afin de mettre en oeuvre les principes de Tests Unitaires et d'Intégration,
nous allons partir de cette base applicative.

Nous avons ici une petite application qui se connecte à l'API Pokédex v2.

Nous obtenons un listing initial de 20 Pokémons,
et nous permettons d'accéder au détail de ces 20 Pokémons.

## Installation

    git clone git@gitlab.enseeiht.fr:mdartigu/tp-testing-pokedex.git
    cd tp-testing-pokedex
    npm install
    npm start
