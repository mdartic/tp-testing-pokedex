module.exports = {
  verbose: true,
  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*.js',
  ],
  clearMocks: true,
  resetMocks: true
};
