module.exports = {
  'Home page render two divs correctly,': function (browser) {
    browser.url('http://localhost:8080');
    browser.expect.element('div.pokemon-detail').to.be.present;
    browser.expect.element('div.pokemon-list').to.be.present;
    browser.screenshot();
  },
  'render a table correctly': function (browser) {
    browser.expect.element('div.pokemon-list table').to.be.present;
    // browser.elements('div.pokemon-list table tr td').assert.containsText('bulbasaur');
  },
  'load a table correctly': function (browser) {
    browser.expect.element('div.pokemon-list table').to.be.present;
    browser.expect.element('div.pokemon-list table tr td button').to.be.present;
    browser.end();
  },
};
